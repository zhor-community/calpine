genFileStructureGo (){
  #!/bin/zsh

# Define directories
directories=(
    "cmd/cugo"
    "pkg/handlers"
    "pkg/models"
    "pkg/routes"
    "pkg/utils"
)

# Create directories
for dir in $directories; do
    mkdir -p cugo/$dir
done

# Create files
touch cugo/go.mod
touch cugo/go.sum
touch cugo/cmd/cugo/main.go
touch cugo/pkg/handlers/csv_handler.go
touch cugo/pkg/handlers/json_handler.go
touch cugo/pkg/handlers/yaml_handler.go
touch cugo/pkg/models/record.go
touch cugo/pkg/routes/routes.go
touch cugo/pkg/utils/file_utils.go

# Display success message
echo "Directory and file structure generated successfully."

}

genFileStructureGo
