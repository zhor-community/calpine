#!/bin/zsh

# Load environment variables
if [ -f .env ]; then
    export $(grep -v '^#' .env | xargs)
else
    echo "Error: .env file not found."
    exit 1
fi

# Function to print an error message and exit
function error_exit {
    echo "Error: $1" | tee -a "$LOG_FILE" >&2
    exit 1
}

# Check if Docker is installed
command -v docker >/dev/null 2>&1 || error_exit "Docker is not installed. Please install Docker and try again."

# Initialize the Swarm
function init_swarm {
    echo "Initializing Docker Swarm..." | tee -a "$LOG_FILE"
    docker swarm init --advertise-addr "$SWARM_ADVERTISE_ADDR" || error_exit "Failed to initialize Docker Swarm."
    echo "Docker Swarm initialized successfully." | tee -a "$LOG_FILE"
}

# Add a Worker
function add_worker {
    local WORKER_TOKEN=$1
    local MANAGER_IP=$2

    if [ -z "$WORKER_TOKEN" ] || [ -z "$MANAGER_IP" ]; then
        error_exit "Worker token or Manager IP is missing."
    fi

    echo "Adding worker to Swarm..." | tee -a "$LOG_FILE"
    docker swarm join --token "$WORKER_TOKEN" "$MANAGER_IP":2377 || error_exit "Failed to add worker to the Swarm."
    echo "Worker added to the Swarm successfully." | tee -a "$LOG_FILE"
}

# Add a Manager
function add_manager {
    local MANAGER_TOKEN=$1
    local MANAGER_IP=$2

    if [ -z "$MANAGER_TOKEN" ] || [ -z "$MANAGER_IP" ]; then
        error_exit "Manager token or Manager IP is missing."
    fi

    echo "Adding manager to Swarm..." | tee -a "$LOG_FILE"
    docker swarm join --token "$MANAGER_TOKEN" "$MANAGER_IP":2377 || error_exit "Failed to add manager to the Swarm."
    echo "Manager added to the Swarm successfully." | tee -a "$LOG_FILE"
}

# Deploy a Stack
function deploy_stack {
    if [ ! -f "$STACK_FILE" ]; then
        error_exit "Stack file '$STACK_FILE' not found."
    fi

    if [ -z "$STACK_NAME" ]; then
        error_exit "Stack name is not specified."
    fi

    echo "Deploying stack '$STACK_NAME'..." | tee -a "$LOG_FILE"
    docker stack deploy -c "$STACK_FILE" "$STACK_NAME" || error_exit "Failed to deploy the stack."
    echo "Stack '$STACK_NAME' deployed successfully." | tee -a "$LOG_FILE"
}

# Check if the node is already part of a Swarm
if docker info | grep -q 'Swarm: active'; then
    echo "This node is already part of a Swarm cluster." | tee -a "$LOG_FILE"
else
    init_swarm
fi

# Instructions for the user
echo "To add a worker, use the add_worker function with the appropriate arguments."
echo "To add a manager, use the add_manager function with the appropriate arguments."
echo "To deploy a stack, use the deploy_stack function."

# Usage examples (uncomment and replace values to use)
# add_worker "$WORKER_TOKEN" "$MANAGER_IP"
# add_manager "$MANAGER_TOKEN" "$MANAGER_IP"
# deploy_stack

