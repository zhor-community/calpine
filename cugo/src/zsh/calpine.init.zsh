#!/usr/bin/env zsh

# Main function for initializing the CRUD application
function init_app {
    # Default values
    local readonly DEFAULT_ADMIN="admin"
    local readonly DEFAULT_REPO="calpine"
    local readonly DEFAULT_VIRT="docker"

    # Function to display usage information
    function show_help {
        echo "Usage: $0 [-a <username>] [-r <repository_name>] [-v <virtualization>]"
        echo "Initialize a standard CRUD application."
        echo ""
        echo "Options:"
        echo "  -a <username>         Specify admin username (default: $DEFAULT_ADMIN)"
        echo "  -r <repository>       Specify repository name (default: $DEFAULT_REPO)"
        echo "  -v <virtualization>   Specify virtualization software (default: $DEFAULT_VIRT)"
        echo "  -h                    Show this help message"
    }

    # Function to initialize the CRUD application
    function initialize_crud {
        local admin="$DEFAULT_ADMIN"
        local repo="$DEFAULT_REPO"
        local virt="$DEFAULT_VIRT"

        # Parse options
        while getopts ":a:r:v:h" opt; do
            case $opt in
                a)
                    admin="$OPTARG"
                    ;;
                r)
                    repo="$OPTARG"
                    ;;
                v)
                    virt="$OPTARG"
                    ;;
                h)
                    show_help
                    return 0
                    ;;
                \?)
                    echo "Error: Invalid option -$OPTARG"
                    show_help
                    return 1
                    ;;
                :)
                    echo "Error: Option -$OPTARG requires an argument."
                    show_help
                    return 1
                    ;;
            esac
        done

        # Shift options to the positional parameters
        shift $((OPTIND - 1))

        # Display chosen parameters
        echo "Initializing CRUD application with:"
        echo "  Admin username: $admin"
        echo "  Repository name: $repo"
        echo "  Virtualization: $virt"

        # Add initialization steps here based on the provided parameters
        echo "CRUD application initialization complete."
    }

    # Call the initialize_crud function with the arguments passed to the script
    initialize_crud "$@"
}

# Validate arguments
if [[ $# -eq 0 ]]; then
    init_app -h
    exit 1
fi

# Call the main function with the arguments passed to the script
init_app "$@"

# Test cases
echo "Running tests..."
#init_app --admin "testadmin" --repo "testrepo" --virt "testvirt"
init_app --admin "anotheradmin"
#init_app --repo "anotherrepo"
#init_app --virt "anothervirt"
#init_app --admin "admin1" --repo "repo1"
#init_app --admin "admin2" --virt "virt2"
#init_app --repo "repo2" --virt "virt2"
echo "Tests complete."

