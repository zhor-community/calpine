#!/bin/zsh


# Default values
myPath="/home/admin/.calpine/"
myGroup="cutopia"
myImage="calpine"
myOperation="start"
myContainer="calpinc"
# Function to manage Docker file with CRUD operations
calpine() {

    # Check if Docker is installed
    if ! command -v docker &>/dev/null; then
      echo "Docker is not installed. Please install Docker before continuing."
      exit 1
    fi

    while getopts ":p:g:i:o:c:" opt; do
        case $opt in
            p) myPath=$OPTARG ;;
            g) myGroup=$OPTARG ;;
            i) myImage=$OPTARG ;;
            o) myOperation=$OPTARG ;;
            c) myContainer=$OPTARG ;;
            \?)
                echo "Invalid option: -$OPTARG" >&2
                exit 1
                ;;
            :)
                echo "Option -$OPTARG requires an argument." >&2
                exit 1
                ;;
        esac
    done

    case $myOperation in
        "start")
            #docker exec -it "$myContainer" zsh
            docker exec -it "$myContainer" sh

        ;;
        "create")
            # Logic for creating Docker file
            echo "Creating Docker iamge: $myImage, with path: $myPath, group: $myGroup, image: container: $myContainer"
            echo "Crating Docker comtainer: $myContainer, "
            echo "Crating from Dockerfiel path: $myPath, "
            echo "Crating for Group : $myGroup. "
            # Check if the image exists
            image_id=$(docker image inspect "$myGroup/$myImage" &>/dev/null)
            if [[ -z "$myImage_id" ]]; then
              # Build the image if it doesn't exist
              docker build -t "$myGroup/$myImage:latest" "$myPath"
            fi

            # Start the container
            docker run -it -d --name "$myContainer" "$myGroup/$myImage"

            ;;
        "read")
            # Logic for reading Docker container
            echo "Test Docker container $myContainer if running"
            # List all containers
            docker ps | grep "$myContainer"

            echo "Test Docker container $myContainer if exist"
            # List all containers
            docker ps -a | grep "$myContainer"

            echo "Test Docker image $myImaage if exist"
            # List all containers
            docker images | grep "$myImage"

            ;;
        "update")
            # Logic for updating Docker file
            echo "Updating Docker container with path: $myPath, group: $myGroup, image: $myImage, container: $myContainer"
             # Update a container
             docker stop "$myContainer"
             docker rm "$myContainer"
             docker run -it -d --name "$myContainer" "$myGroup/$myImage"

            ;;

        "upgrade")
            echo "Updating Docker container with path: $myPath, group: $myGroup, image: $myImage, container: $myContainer"
             # Delete a container
             docker stop "$myContainer"
             docker rm "$myContainer"
             docker rmi "$myGroup/$myImage"
             docker build -t "$myGroup/$myImage" "$myPath"
             docker run -it -d -p 2222:22 --name "$myContainer" "$myGroup/$myImage"
             #docker exec -it $myContainer zsh
             docker exec -it $myContainer sh


            ;;

        "delete")
            # Logic for deleting Docker file
            echo "Deleting Docker image & container  with path: $myPath, group: $myGroup, image: $myImage, container: $myContainer"
            # Delete a container
            docker stop "$myContainer"
            docker rm "$myContainer"
            docker rmi "$mygroup/$myimage"

            ;;
        *)
            echo "Invalid operation: $myOperation. Supported operations: create, read, update, delete." >&2
            exit 1
            ;;
    esac
}

# Call the function with command line arguments
# manageDockerFile "$@"
#
# calpineManager -p "$myPath" -g "$myGroup" -i "$myImage" -o "upgrade" -c "$myContainer"
#zerozone() {
            # calpineManager -p "$myPath" -g "$myGroup" -i "$myImage" -o "upgrade" -c "$myContainer"

#    docker compose --project-name docker up



#}
#zerozone
