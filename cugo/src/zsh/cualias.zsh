# ~/.zshrc
#
# Navigation Aliases
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias ~="cd ~"
alias c="clear"
alias h="cd ~"  # Quick access to home directory
alias d="cd -"

# Directory Listing Aliases
alias ll="ls -lhF"  # Long format with human-readable sizes and file type indicators
alias la="ls -A"  # List all files except . and ..
alias l="ls -CF"  # List in columns, show file type indicators

# Git Aliases
alias g="git"
alias ga="git add"
alias gaa="git add --all"
alias gb="git branch"
alias gba="git branch -a"
alias gcm="git commit -m"
alias gco="git checkout"
alias gd="git diff"
alias gs="git status"
alias gp="git pull"
alias gpu="git push"
alias gpl="git pull"
alias gl="git log --oneline --graph --decorate --all"
alias gm="git merge"
alias gr="git rebase"
alias gst="git stash"
alias gsp="git stash pop"
alias gcl="git clone"
alias gclean="git clean -fd"

# Docker Aliases
alias d="docker"
alias dps="docker ps"
alias di="docker images"
alias drm="docker rm $(docker ps -a -q)"  # Remove all containers
alias drmi="docker rmi $(docker images -q)"  # Remove all images
alias dcb="docker-compose build"
alias dc="docker-compose"
alias dcu="docker-compose up"
alias dcd="docker-compose down"
alias dlogs="docker logs"
alias dexec="docker exec -it"

# System Aliases
alias update="sudo apt update && sudo apt upgrade -y"  # Update for Debian/Ubuntu systems
alias please="sudo"
alias reload="source ~/.zshrc"  # Reload the .zshrc file
alias ehosts="sudo nvim /etc/hosts"  # Edit the hosts file
alias top="htop"  # Use htop instead of top if installed

# Network Aliases
alias ipP="curl ifconfig.me"  # Display public IP address
alias ips="ip a"  # Display all network interfaces and IP addresses
alias pingg="sudo ping -c 4 google.com"
alias reNet="sudo systemctl restart networking"
# Security Aliases
alias genpasswd="openssl rand -base64 12"  # Generate a random password of 12 characters

# Archive Management Aliases
alias untar="tar -xvf"
alias targz="tar -czvf"
alias untargz="tar -xzvf"

# Python Aliases
#alias py="python3"
#alias pyenv="source venv/bin/activate"  # Activate Python virtual environment

# Kubernetes Aliases
alias k="kubectl"
alias kgp="kubectl get pods"
alias kgd="kubectl get deployments"
alias kgs="kubectl get services"
alias kctx="kubectl config use-context"
alias kdel="kubectl delete"
alias klogs="kubectl logs"

# Enhancements for Common Tasks
alias mkdir="mkdir -pv"  # Create directories and print the path
alias grep="grep --color=auto"  # Highlight matches
alias diff="diff --color=auto"  # Colored diff output

# Improved Command Line Experience
alias ls="ls --color=auto"  # Colored output for ls
alias df="df -h"  # Human-readable disk usage
alias free="free -m"  # Memory usage in megabytes

# Alias for Quick Edits
alias nvimrc="nvim ~/.cudit/"
alias zshrc="nvim ~/.zshrc"
alias aliases="nvim ~/.zshrc"  # Directly open this alias section for quick edits

