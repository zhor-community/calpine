#!/bin/zsh

# Default values
myOperation="start" #start create read update delette backup stop
myProjectName="site" #OpenZITI Zhor zerozone 
myType="frontend" # backend api Portfolio Portail  
myLang="nodejs" # nodejs php lua python
myDB="postgresql" # sqlite mariadb couchdb
myBC="hyperladger" #solana etherium hyperladger
myServer="caddy" # caddy apache nginx 
myENVFILE="yats.env" # yats calpine cutopia ticenergy zhorc

# Function to manage Docker file with CRUD operations
cuapp() {





    # Check if Docker is installed
    if ! command -v docker &>/dev/null; then
      echo "Docker is not installed. Please install Docker before continuing."
      exit 1
    fi

    while getopts ":o:p:d:e:" opt; do
        case $opt in
            o) myOperation=$OPTARG ;;
            p) myProjectName=$OPTARG ;;
            d) myDockerComposeFile=$OPTARG ;;
            e) myENVFILE=$OPTARG ;;
            \?)
                echo "Invalid option: -$OPTARG" >&2
                exit 1
                ;;
            :)
                echo "Option -$OPTARG requires an argument." >&2
                exit 1
                ;;
        esac
    done

    case $myOperation in
        "start")

        ;;
        "create")

            ;;
        "read")

            ;;
        "update")

            ;;

        "upgrade")

            ;;

        "delete")

            ;;
        "backup")

            ;;
        "stop")

            ;;
        *)
            echo "Invalid operation: $myOperation. Supported operations: create, read, update, delete." >&2
            exit 1
            ;;
    esac
}
