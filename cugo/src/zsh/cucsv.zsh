cucsv() {
    local action="$1"
    local csv_file="$2"
    local line_number
    local column_number
    local value

    shift 2

    while getopts ":l:c:v:" opt; do
        case $opt in
            l) line_number="$OPTARG";;
            c) column_number="$OPTARG";;
            v) value="$OPTARG";;
            \?) echo "Option invalide: -$OPTARG" >&2; return 1;;
            :) echo "L'option -$OPTARG requiert un argument." >&2; return 1;;
        esac
    done

    case "$action" in
        view)
            cat "$csv_file" | column -s, -t | less
            ;;
        add)
            if [[ -z "$line_number" || -z "$column_number" || -z "$value" ]]; then
                echo "Usage: cucsv add [csv_file] -l [line_number] -c [column_number] -v [value]"
                return 1
            fi
            awk -v line="$line_number" -v column="$column_number" -v val="$value" 'BEGIN {FS=OFS=","} NR == line {$column = val} 1' "$csv_file" > temp && mv temp "$csv_file"
            ;;
        delete)
            if [[ -z "$line_number" ]]; then
                echo "Usage: cucsv delete [csv_file] -l [line_number]"
                return 1
            fi
            sed -i "${line_number}d" "$csv_file"
            ;;
        search)
            if [[ -z "$value" ]]; then
                echo "Usage: cucsv search [csv_file] -v [value]"
                return 1
            fi
            grep -n "$value" "$csv_file"
            ;;
        edit)
            if [[ -z "$line_number" || -z "$value" ]]; then
                echo "Usage: cucsv edit [csv_file] -l [line_number] -v [new_value]"
                return 1
            fi
            sed -i "${line_number}s/.*/$value/" "$csv_file"
            ;;
        *)
            echo "Usage: cucsv {view|add|delete|search|edit} [csv_file] -l [line_number] -c [column_number] -v [value]"
            ;;
    esac
}

