#source cusource.zsh
#RUN apk add zsh git tree sudo neovim ranger wget curl go gnupg nodejs yarn npm openrc 
#RUN apk --no-cache add zsh curl git sudo openrc wget
############################################################################
#                                                                          #
#                                 Pkgs                                     #
#                                                                          #
############################################################################
apk update && \
    apk add --no-cache --upgrade \
                   alpine-sdk \
                   tini \
                   zsh \
                   git \
                   tree \
                   sudo \
                   neovim \
                   ranger \
                   wget \
                   curl \
                   go \
                   gnupg2 \
                   nodejs \
                   yarn \
                   npm \
                   openrc \
                   bash \
                   curl \
                   neovim \
                   git \
                   ca-certificates \
                   fzf
                   # Ensure all scripts and binaries are executable
                   chmod +x /usr/local/bin/* && \
                   # Clean up to reduce image size
                   rm -rf /var/cache/apk/* /tmp/* /var/tmp/*
############################################################################
#                                                                          #
#                                 sudo                                     #
#                                                                          #
############################################################################
adduser -D admin
echo "admin ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
#RUN usermod -aG sudo username
#RUN    addgroup -S cutopia #&& \

#############################################################################
#                                                                           #
#                                                                           #
#                          cutopia/calpine/zshrc                            #
#                                                                           #
#                                                                           #
#############################################################################
mkdir -p /home/admin/.antigen
curl -L git.io/antigen > /home/admin/.antigen/antigen.zsh
chown -R admin:admin /home/admin/.antigen
mkdir -p /home/admin/calpine
cp /app/* /home/admin/calpine
chown -R admin:admin /home/admin/calpine
ln -s /home/admin/calpine/zsh/zshrc /home/admin/.zshrc
chown -R admin:admin /home/admin/.zshrc
############################################################################
#                                                                          #
#                               openssh                                    #
#                                                                          #
############################################################################
#apk add openssh
#echo 'PasswordAuthentication yes' >> /etc/ssh/sshd_config
#mkdir /run/openrc/
#touch /run/openrc/softlevel
#rc-updaggte add sshd
#rc-status
#rc-service sshd start
#ssh-keygen -A
#exec /usr/sbin/sshd -D & #-e "$@"
#rc-status
#rc-service sshd start
##rc-service sshd restart
zsh /app/src/cussh/init.zsh
######################################################################### ##
#                                                                          #
#                                Docker                                    #
#                                                                          #
############################################################################
#https://wiki.alpinelinux.org/wiki/Docker#Installation
#apk add docker
#apk add docker-cli-compose
#sudo addgroup $USER docker
#rc-update add docker default
#service docker start

############################################################################
#                                                                          #
#                               calpine                                    #
#                                                                          #
############################################################################
#ssh-keygen -t ed25519 -C "abdelhakimzouai@gmail.com"
#eval "$(ssh-agent -s)"
#ssh-add ~/.ssh/id_ed25519
#cat ~/.ssh/id_ed25519.pub
# Then, go to GitLab and navigate to:
#
#User Settings > SSH Keys
#Paste the copied SSH key into the "Key" field and give it a relevant title.
#Click "Add key".
#ssh -T git@gitlab.com
#git clone git@gitlab.com:zhor-community/calpine.git ~/.calpine
#
#############################################################################
#                                                                          #
#                                 zsh                                      #
#                                                                          #
############################################################################
#cuapk zsh
#chsh -s $(which zsh)
#
#sudo apk add shadow
#sudo chsh -s /bin/zsh
#chsh -s /bin/zsh
#sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
#mkdir ~/.antigen/
#curl -L git.io/antigen > ~/.antigen/antigen.zsh
#ln -s ~/.calpine/zsh/zshrc ~/.zshrc


#############################################################################
#                                                                          #
#                               caddy                                      #
#                                                                          #
############################################################################
#https://chatgpt.com/share/6143cbdc-ec50-48dd-b569-ad3fdadd5fc7
#curl -sS https://webi.sh/caddy | sh
#source ~/.config/envman/PATH.env
#caddy start


############################################################################
#                                                                          #
#                              apache2                                     #
#                                                                          #
############################################################################
#https://wiki.alpinelinux.org/wiki/Apache
#apk add apache2
#rc-service apache2 start
#rc-update add apache2
#rc-service apache2 restart

#https://wiki.alpinelinux.org/wiki/Apache_with_php-fpm
#apk add apache2-proxy php8-fpm
#rc-service php-fpm8 start
#rc-update add php-fpm8
#rc-service apache2 start
#rc-update add apache2


#############################################################################
#                                                                          #
#                                python                                    #
#                                                                          #
############################################################################
zsh cupy.zsh
zsh django.zsh


#############################################################################
#                                                                          #
#                                php                                       #
#                                                                          #
############################################################################
#cuapk create php


#############################################################################
#                                                                          #
#                            nextcloud                                     #
#                                                                          #
############################################################################
#NEXTBRANCH=
#git clone git@github.com:nextcloud/server.git --branch $NEXTBRANCH
#cd server
#git submodule update --init

############################################################################
#                                                                          #
#                            e-learing tools                               #
#                                                                          #
############################################################################
cuapk add screenkey onboard
