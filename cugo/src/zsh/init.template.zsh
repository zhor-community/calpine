
#!/usr/bin/env zsh

# Main function for initializing the CRUD application
function init_app {
    # Default values for options
    local default_admin="admin" # hermit 
    local default_repo="calpine" # cutopia
    local default_virt="docker"  # local docker lxc kq xen

    # Variables to store options
    local admin="$default_admin"
    local repo="$default_repo"
    local virt="$default_repo"

    # Function to display usage information
    function show_help {
        echo "Usage: $0 [--admin <username>] [--repo <repository_name>]"
        echo "Initialize a standard CRUD application."
        echo ""
        echo "Options:"
        echo "  --admin <username>   Specify admin username (default: $default_admin)"
        echo "  --repo <repository>  Specify repository name (default: $default_repo)"
        echo "  --virt software  Specify soft of virt name (default: $default_repo)"
        echo "  -h, --help           Show this help message"
    }

    # Parse command line arguments
    while [[ $# -gt 0 ]]; do
        case "$1" in
            --admin)
                admin="$2"
                shift 2
                ;;
            --repo)
                repo="$2"
                shift 2
                ;;
            --virt)
                virt="$2"
                shift 2
                ;;
            -h|--help)
                show_help
                return 0
                ;;
            *)
                echo "Error: Unknown option $1"
                show_help
                return 1
                ;;
        esac
    done

    # Display chosen parameters
    echo "Initializing CRUD application with:"
    echo "  Admin username: $admin"
    echo "  Repository name: $repo"
    echo "  Virtualisation by : $virt"

    # Add your initialization steps here based on the provided parameters
    # For example:
    # - Clone repository
    # - Setup admin configuration
    # - Initialize database
    # - Configure environment, etc.

    # Example usage of these parameters with your application
    # zsh scripts/init_app.zsh --admin "$admin" --repo "$repo"

    echo "CRUD application initialization complete."
}

# Call the main function with the arguments passed to the script
#init_app "$@"
#
# Test cases
echo "Running tests..."
#init_app --admin "testadmin" --repo "testrepo" --virt "testvirt"
init_app --admin "anotheradmin"
#init_app --repo "anotherrepo"
#init_app --virt "anothervirt"
#init_app --admin "admin1" --repo "repo1"
#init_app --admin "admin2" --virt "virt2"
#init_app --repo "repo2" --virt "virt2"
echo "Tests complete."

