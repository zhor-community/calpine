function netinfo() {
    if [ -z "$1" ]; then
        echo "Usage: netinfo <interface>"
    else
        echo "Information for interface: $1"
        ip addr show dev "$1"
        ip route show dev "$1"
    fi
}
function multiPing() {
    for site in choof.ooredoo.dz google.com github.com stackoverflow.com; do
        echo "---------------------------"
        echo "Pinging $site..."
        ping -c 4 $site
        echo "---------------------------"
    done
}
function openPorts() {
    echo "Open ports:"
    ss -tuln | grep LISTEN
}
function reNetwork() {
    echo "Restarting network service..."
    systemctl restart networking.service
    echo "Network restarted."
}

function ipPublic() {
    echo "Fetching public IP address..."
    curl -s https://ifconfig.me
    echo ""
}
########################################################
#                                                      #
#                                                      #
#                          DNS                         #
#                                                      #
#                                                      #
########################################################
function dns_lookup() {
    if [ -z "$1" ]; then
        echo "Usage: dns_lookup <domain>"
    else
        echo "Resolving domain: $1"
        dig +short "$1"
    fi
}
function dns_record() {
    if [ -z "$1" ] || [ -z "$2" ]; then
        echo "Usage: dns_record <record_type> <domain>"
        echo "Example: dns_record MX example.com"
    else
        echo "Checking $1 record for domain: $2"
        dig "$2" "$1" +short
    fi
}
function flush_dns() {
    echo "Flushing DNS cache..."
    sudo systemd-resolve --flush-caches
    echo "DNS cache flushed."
}
function flush_dnsmasq() {
    echo "Flushing DNS cache..."
    sudo service dnsmasq restart
    echo "DNS cache flushed."
}
function check_dns_propagation() {
    if [ -z "$1" ]; then
        echo "Usage: check_dns_propagation <domain>"
    else
        echo "Checking DNS propagation for domain: $1"
        for server in 8.8.8.8 1.1.1.1 9.9.9.9; do
            echo "Checking with DNS server: $server"
            dig @"$server" "$1" +short
        done
    fi
}
function dns_nslookup() {
    if [ -z "$1" ]; then
        echo "Usage: dns_nslookup <domain>"
    else
        echo "Fetching DNS servers for domain: $1"
        dig NS "$1" +short
    fi
}
function dns_all_records() {
    if [ -z "$1" ]; then
        echo "Usage: dns_all_records <domain>"
    else
        echo "Fetching all DNS records for domain: $1"
        dig "$1" ANY +noall +answer
    fi
}
function dns_test_server() {
    if [ -z "$1" ] || [ -z "$2" ]; then
        echo "Usage: dns_test_server <dns_server> <domain>"
        echo "Example: dns_test_server 8.8.8.8 example.com"
    else
        echo "Testing DNS resolution for $2 using server $1"
        dig @"$1" "$2" +short
    fi
}

