#!/bin/zsh

# Default values
#myOperation="start"  # start create read update delete backup stop
#myProjectName="yats"  # OpenZITI Zhor zerozone
#myDockerComposeFile="docker-compose.yats.yml"
#myENVFILE="yats.env"

# Function to manage Docker file with CRUD operations
yats() {
    # Default values
    local myOperation="start"  # start create read update delete backup stop
    local myProjectName="yats"  # OpenZITI Zhor zerozone
    local myDockerComposeFile="docker-compose.yats.yml"
    local myENVFILE="yats.env"

    # Check if Docker is installed
    if ! command -v docker &>/dev/null; then
        echo "Docker is not installed. Please install Docker before continuing."
        exit 1
    fi

    # Parse options
    while getopts ":o:p:d:e:" opt; do
        case $opt in
            o) myOperation=$OPTARG ;;
            p) myProjectName=$OPTARG ;;
            d) myDockerComposeFile=$OPTARG ;;
            e) myENVFILE=$OPTARG ;;
            \?)
                echo "Invalid option: -$OPTARG" >&2
                exit 1
                ;;
            :)
                echo "Option -$OPTARG requires an argument." >&2
                exit 1
                ;;
        esac
    done

    # Check if Docker Compose file and environment file exist
    if [[ ! -f "$myDockerComposeFile" || ! -f "$myENVFILE" ]]; then
        echo "Docker Compose file or environment file not found!"
        exit 1
    fi

    # Perform the requested operation
    case $myOperation in
        "start")
            docker-compose -f "$myDockerComposeFile" --env-file "$myENVFILE" up -d
            echo "Docker Compose project '$myProjectName' started."
            ;;
        "create")
            docker-compose -f "$myDockerComposeFile" --env-file "$myENVFILE" up -d --build
            echo "Docker Compose project '$myProjectName' created and started."
            ;;
        "read")
            docker-compose -f "$myDockerComposeFile" --env-file "$myENVFILE" ps
            ;;
        "update")
            docker-compose -f "$myDockerComposeFile" --env-file "$myENVFILE" up -d --build
            echo "Docker Compose project '$myProjectName' updated."
            ;;
        "upgrade")
            docker-compose -f "$myDockerComposeFile" --env-file "$myENVFILE" pull
            docker-compose -f "$myDockerComposeFile" --env-file "$myENVFILE" up -d
            echo "Docker Compose project '$myProjectName' upgraded."
            ;;
        "delete")
            docker-compose -f "$myDockerComposeFile" --env-file "$myENVFILE" down
            echo "Docker Compose project '$myProjectName' deleted."
            ;;
        "backup")
            docker-compose -f "$myDockerComposeFile" --env-file "$myENVFILE" exec -T <service_name> sh -c "pg_dump -U <username> <database> > /backup/<backup_file>"
            echo "Backup for Docker Compose project '$myProjectName' created."
            ;;
        "stop")
            docker-compose -f "$myDockerComposeFile" --env-file "$myENVFILE" down
            echo "Docker Compose project '$myProjectName' stopped."
            ;;
        *)
            echo "Invalid operation: $myOperation. Supported operations: start, create, read, update, upgrade, delete, backup, stop." >&2
            exit 1
            ;;
    esac
}

# Calling the dztartup function with the provided arguments
# yats "$@"
