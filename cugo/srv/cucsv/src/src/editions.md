
| Application            | Famille       | Professionnel | Entreprise | Startup | Sécurité/Piratage | DevOps  | Installation Préférée                           | Base de Données Préférée |
|------------------------|---------------|--------------|------------|---------|-------------------|---------|------------------------------------------------|-------------------------|
| Docker                 | Outils        | Recommandé   | Recommandé | Recommandé | Non recommandé  | Recommandé | Commenté : `apk add docker`                   | NoDB                    |
| Node.js et npm         | Développement | Recommandé   | Recommandé | Recommandé | Non recommandé  | Recommandé | Paquet : `nodejs (apk add nodejs)`             | NoDB                    |
| Yarn                   | Développement | Recommandé   | Recommandé | Recommandé | Non recommandé  | Recommandé | Installation globale : `npm install -g yarn`   | NoDB                    |
| QuasarJS (globalement) | Développement | Recommandé   | Recommandé | Recommandé | Non recommandé  | Recommandé | Installation globale : `yarn global add @quasar/cli` | NoDB                    |
| Express                | Développement | Recommandé   | Recommandé | Recommandé | Non recommandé  | Recommandé | Installation : `yarn install express`         | NoDB                    |
| Mongoose               | Développement | Recommandé   | Recommandé | Recommandé | Non recommandé  | Recommandé | Installation : `yarn install mongoose`        | MongoDB                 |
| Bcryptjs               | Développement | Recommandé   | Recommandé | Recommandé | Non recommandé  | Recommandé | Installation : `yarn install bcryptjs`        | NoDB                    |
| Jsonwebtoken           | Développement | Recommandé   | Recommandé | Recommandé | Non recommandé  | Recommandé | Installation : `yarn install jsonwebtoken`    | NoDB                    |
| Helmet                 | Développement | Recommandé   | Recommandé | Recommandé | Non recommandé  | Recommandé | Installation : `yarn install helmet`          | NoDB                    |
| Express-session        | Développement | Recommandé   | Recommandé | Recommandé | Non recommandé  | Recommandé | Installation : `yarn install express-session`  | NoDB                    |
| Body-parser            | Développement | Recommandé   | Recommandé | Recommandé | Non recommandé  | Recommandé | Installation : `yarn install body-parser`      | NoDB                    |
| Cors                   | Développement | Recommandé   | Recommandé | Recommandé | Non recommandé  | Recommandé | Installation : `yarn install cors`            | NoDB                    |
| Axios                  | Développement | Recommandé   | Recommandé | Recommandé | Non recommandé  | Recommandé | Installation : `yarn install axios`           | NoDB                    |
| Moment                 | Développement | Recommandé   | Recommandé | Recommandé | Non recommandé  | Recommandé | Installation : `yarn install moment`          | NoDB                    |
| Chart.js               | Développement | Recommandé   | Recommandé | Recommandé | Non recommandé  | Recommandé | Installation : `yarn install chart.js`        | NoDB                    |
| Sweetalert2            | Développement | Recommandé   | Recommandé | Recommandé | Non recommandé  | Recommandé | Installation : `yarn install sweetalert2`     | NoDB                    |
| Nodemailer             | Développement | Recommandé   | Recommandé | Recommandé | Non recommandé  | Recommandé | Installation : `yarn install nodemailer`      | NoDB                    |
| Fzf                    | Outils        | Recommandé   | Recommandé | Recommandé | Non recommandé  | Recommandé | Paquet : `fzf (apk add fzf)`                   | NoDB                    |
| Git                    | Outils        | Recommandé   | Recommandé | Recommandé | Non recommandé  | Recommandé | Paquet : `git (apk add git)`                   | NoDB                    |
| Neovim                 | Outils        | Recommandé   | Recommandé | Recommandé | Non recommandé  | Recommandé | Paquet : `neovim (apk add neovim)`             | NoDB                    |
| Tree                   | Outils        | Recommandé   | Recommandé | Recommandé | Non recommandé  | Recommandé | Paquet : `tree (apk add tree)`                 | NoDB                    |
| Ranger                 | Outils        | Recommandé   | Recommandé | Recommandé | Non recommandé  | Recommandé | Paquet : `ranger (apk add ranger)`             | NoDB                    |
| Ansible                | Outils        | Recommandé   | Recommandé | Recommandé | Non recommandé  | Commenté : `apk add ansible`                  | NoDB                    |
| Go                     | Outils        | Recommandé   | Recommandé | Recommandé | Non recommandé  | Paquet : `go (apk add go)`                     | NoDB                    |
| OpenSSH Server         | Sécurité      | Recommandé   | Recommandé | Recommandé | Non recommandé  | Paquet : `openssh (apk add openssh)`           | NoDB                    |
| OpenSSL                | Sécurité      | Recommandé   | Recommandé | Recommandé | Non recommandé  | Paquet : `openssl (apk add openssl)`           | NoDB                    |
| GnuPG                  | Sécurité      | Recommandé   | Recommandé | Recommandé | Non recommandé  | Paquet : `gnupg (apk add gnupg)`               | NoDB                    |
| OpenVPN                | Sécurité      | Non recommandé | Non recommandé | Non recommandé | Non recommandé  | Commenté : `apk add openvpn`                  | NoDB                    |
| Bitcoin                | Blockchain   | Recommandé   | Recommandé | Recommandé | Non recommandé  | Paquet : `bitcoind (apk add bitcoind)`, `bitcoin-cli (apk add bitcoin-cli)` | Bitcoin (base de données) |
| Ethereum               | Blockchain   | Recommandé   | Recommandé | Recommandé | Non recommandé  | Paquet : `ethereum (apk add ethereum)`, `ethereum-cli (apk add ethereum-cli)` | Ethereum (base de données) |
| Loki                   | Outils        | Recommandé   | Recommandé | Recommandé | Non recommandé  | Paquet : `loki (apk add loki)`, `loki-cli (apk add loki-cli)` | NoDB                    |
| Application            | Famille       | Professionnel | Entreprise | Startup | Sécurité/Piratage | DevOps  | Installation Préférée                           | Base de Données Préférée |
|------------------------|---------------|--------------|------------|---------|-------------------|---------|------------------------------------------------|-------------------------|
| Monero                 | Blockchain   | Recommandé   | Recommandé | Recommandé | Non recommandé  | Recommandé | Paquet : `monero (apk add monero)`, `monero-cli (apk add monero-cli)` | Monero (base de données) |
| GitLab CLI             | Outils        | Recommandé   | Recommandé | Recommandé | Non recommandé  | Recommandé | Paquet : `gitlab-cli (apk add gitlab-cli)`   | NoDB                    |
| Quasar CLI             | Développement | Recommandé   | Recommandé | Recommandé | Non recommandé  | Recommandé | Paquet : `quasar-cli (apk add quasar-cli)`   | NoDB                    |
| Nextcloud              | Outils        | Recommandé   | Recommandé | Recommandé | Non recommandé  | Recommandé | Paquet : `nextcloud (apk add nextcloud)`     | NoDB                    |
| Friendica              | Réseau social | Non recommandé | Non recommandé | Non recommandé | Recommandé       | -          |
| Gitea                  | Outils        | Non recommandé | Non recommandé | Non recommandé | Recommandé       | -          |
| Hyperledger Fabric     | Blockchain   | Non recommandé | Non recommandé | Non recommandé | Recommandé       | -          |
| Hyperledger Indy       | Blockchain   | Non recommandé | Non recommandé | Non recommandé | Recommandé       | -          |
| Jitsi                  | Communication | Non recommandé | Non recommandé | Non recommandé | Recommandé       | -          |
| Magento                | E-commerce    | Non recommandé | Non recommandé | Non recommandé | Recommandé       | -          | MySQL                   |
| Mattermost             | Communication | Non recommandé | Non recommandé | Non recommandé | Recommandé       | -          | MySQL                   |
| Moodle                 | Éducation     | Non recommandé | Non recommandé | Non recommandé | Recommandé       | -          | MySQL                   |
| NetData                | Outils        | Non recommandé | Non recommandé | Non recommandé | Recommandé       | -          |
| OpenLDAP               | Outils        | Non recommandé | Non recommandé | Non recommandé | Recommandé       | -          | LDAP                    |
| OpenVPN                | Sécurité      | Non recommandé | Non recommandé | Non recommandé | Recommandé       | -          |
| phpMyAdmin             | Outils        | Non recommandé | Non recommandé | Non recommandé | Recommandé       | -          | MySQL                   |
| Pi-Hole                | Sécurité      | Non recommandé | Non recommandé | Non recommandé | Recommandé       | -          |
| PostgreSQL             | Base de données | Non recommandé | Non recommandé | Non recommandé | Recommandé       | PostgreSQL              |
| Syncthing              | Outils        | Non recommandé | Non recommandé | Non recommandé | Recommandé       | -          |
| Certbot                | Sécurité      | Non recommandé | Non recommandé | Non recommandé | Recommandé       | -          |
| Tahoe-LAFS             | Stockage      | Non recommandé | Non recommandé | Non recommandé | Recommandé       | -          |
| IPFS                   | Stockage      | Non recommandé | Non recommandé | Non recommandé | Recommandé       | -          |
| Metasploit             | Sécurité      | Non recommandé | Non recommandé | Non recommandé | Recommandé       | -          | PostgreSQL              |
| Wireshark              | Sécurité      | Non recommandé | Non recommandé | Non recommandé | Recommandé       | -          |
| Nmap                   | Sécurité      | Non recommandé | Non recommandé | Non recommandé | Recommandé       | -          |
| Burp Suite             | Sécurité      | Non recommandé | Non recommandé | Non recommandé | Recommandé       | -          |
| OWASP ZAP              | Sécurité      | Non recommandé | Non recommandé | Non recommandé | Recommandé       | -          |
| John the Ripper        | Sécurité      | Non recommandé | Non recommandé | Non recommandé | Recommandé       | -          |
| SQLMap                 | Sécurité      | Non recommandé | Non recommandé | Non recommandé | Recommandé       | -          |
| Hydra                  | Sécurité      | Non recommandé | Non recommandé | Non recommandé | Recommandé       | -          |
| Osquery                | Sécurité      | Non recommandé | Non recommandé | Non recommandé | Recommandé       | -          |
| Snort                  | Sécurité      | Non recommandé | Non recommandé | Non recommandé | Recommandé       | -          |
| Tcpdump                | Sécurité      | Non recommandé | Non recommandé | Non recommandé | Recommandé       | -          |
| Aircrack-ng            | Sécurité      | Non recommandé | Non recommandé | Non recommandé | Recommandé       | -          |
| Hashcat                | Sécurité      | Non recommandé | Non recommandé | Non recommandé | Recommandé       | -          |
| Tor                    | Sécurité      | Non recommandé | Non recommandé | Non recommandé | Recommandé       | -          |
| Proxychains-NG         | Sécurité      | Non recommandé | Non recommandé | Non recommandé | Recommandé       | -          |
| Volatility             | Sécurité      | Non recommandé | Non recommandé | Non recommandé | Recommandé       | -          |
| dovecot                | Outils        | Recommandé   | Recommandé | Recommandé | Recommandé       | Paquet : dovecot (apt-get, yum, etc.), Dockerfile, Docker Compose, Ansible | NoDB                    |

