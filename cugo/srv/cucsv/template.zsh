#!/bin/zsh
# Default values
myENVFILE="" #start create read update delette backup stop
myJsonFile="yats" #OpenZITI Zhor zerozone 
myRepo="~/calpine"
myORG="cutopia"

# Function to manage Docker file with CRUD operations
dztartup() {





    # Check if Docker is installed
    if ! command -v docker &>/dev/null; then
      echo "Docker is not installed. Please install Docker before continuing."
      exit 1
    fi

    while getopts ":o:p:s:e:" opt; do
        case $opt in
            o) myOperation=$OPTARG ;;
            p) myProjectName=$OPTARG ;;
            s) myStartup=$OPTARG ;;
            e) myENVFILE=$OPTARG ;;
            \?)
                echo "Invalid option: -$OPTARG" >&2
                exit 1
                ;;
            :)
                echo "Option -$OPTARG requires an argument." >&2
                exit 1
                ;;
        esac
    done

    case $myOperation in
        "start")

        ;;
        "create")

            ;;
        "read")

            ;;
        "update")

            ;;

        "upgrade")

            ;;

        "delete")

            ;;
        "backup")

            ;;
        "stop")

            ;;
        *)
            echo "Invalid operation: $myOperation. Supported operations: create, read, update, delete." >&2
            exit 1
            ;;
    esac
}
