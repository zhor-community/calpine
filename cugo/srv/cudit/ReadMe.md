# Neovim Configuration - Cudit

Welcome to the Neovim Configuration - Cudit repository. This project provides a customized Neovim configuration, easy to install and use.

## Prerequisites

Before you begin, ensure you have the following installed on your system:

- **Git**
- **Zsh**

## Installation

To install the configuration, follow these steps:

1. Clone the GitLab repository into the directory of your choice. Here, we use `~/.cudit`:

   ```sh
   git clone git@gitlab.com:zhor-community/cudit.git ~/.cudit
   ```

2. Navigate to the cloned directory:

   ```sh
   cd ~/.cudit
   ```

3. Run the initialization script to set up Neovim:

   ```sh
   zsh init.zsh
   ```

## Usage

After installation, launch Neovim to enjoy your new configuration:

```sh
nvim

```

# Donations

To support this project, you can make a donation to its current maintainer:

### Bitcoin

[![Donate via Bitcoin](donabella/img/bitcoin-donate-black.png)](donabella/wallet/Bitcoin)
[![Donate via Bitcoin QR Code](donabella/img/bitcoinQR.svg)](donabella/wallet/Bitcoin)

### Solana

[![Donate via Solana](donabella/img/donateSolana.svg)](donabella/wallet/Solana)
[![Donate via Solana QR Code](donabella/img/solanaQR.svg)](donabella/wallet/Solana)

### Ethereum

[![Donate via Ethereum](donabella/img/donateEthereum.svg)](donabella/wallet/Ethereum)
[![Donate via Ethereum QR Code](donabella/img/ethereumQR.svg)](donabella/wallet/Ethereum)

### Polygon

[![Donate via Polygon](donabella/img/donatePolygon.svg)](donabella/wallet/Polygon)
[![Donate via Polygon QR Code](donabella/img/polygonQR.svg)](donabella/wallet/Polygon)

### Binance

[![Donate via Binance](donabella/img/donateBinance.svg)](donabella/wallet/Binance)
[![Donate via Binance QR Code](donabella/img/binanceQR.png)](donabella/wallet/Binance)
