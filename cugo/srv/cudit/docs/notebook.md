# Manuel des Mappings pour Neovim

Ce manuel décrit les différents raccourcis clavier configurés pour Neovim en utilisant Lua.

## Fonction de mappage

### telescope 

* <leader>ff : Recherche de fichiers.
* <leader>fg : Recherche en direct dans le contenu des fichiers.
* <leader>fb : Navigation entre les buffers ouverts.
* <leader>fh : Recherche des étiquettes d'aide.
* <leader>fs : Affichage du statut Git du projet.
* <leader>fc : Recherche des commits Git dans le projet.  
