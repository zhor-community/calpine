#myPath="/home/admin/.calpine/"
source zsh/zshrc
myPath="."
myGroup="cutopia"
myImage="calpine"
myOperation="update"
myContainer="calpinc"

calpine -p "$myPath" -g "$myGroup" -i "$myImage" -o "upgrade" -c "$myContainer"
